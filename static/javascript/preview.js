//Ewww globals...

var image_list = [{}];
var canvas_image = {url:"/static/images/resized_upright_power_7.png", x:300-202, y:300-202, width:405, height:405, visible:true};
var custom_mask_image = null;
var context, canvas;
var _URL = window.URL || window.webkitURL;
var is_previewing_mask = false;
var back_mask_image = new Image();
var showing_advanced_features = false;
back_mask_image.src = "/static/images/resized_base_mask_upright.png";

var default_backgrounds = [
    "/static/images/resized_power_upright_7.png",
    "/static/images/resized_speed_upright_7.png",
    "/static/images/resized_magic_upright_7.png",
    "/static/images/resized_power_reversed_7.png",
    "/static/images/resized_speed_reversed_7.png",
    "/static/images/resized_magic_reversed_7.png"
];

//Used to keep track of the mouse and image state
var mouse_pressed = false;
var mouse_last_pos = {};
var dragging_image = false;
var dragged_image = null;

function modifyImage(index, url, x, y, width, height, locked, raw)
{
    image_list[index].url = url;
    image_list[index].width = width;
    image_list[index].height = height;
    image_list[index].locked = locked;
    image_list[index].raw = raw;
    image_list[index].x = x;
    image_list[index].y = y;
}

function scale_image(img, basewidth){
    //Rescale in image maintaining the same ratio based on the width
    wpercent = (basewidth / img.width);
    hsize = parseInt(img.height * wpercent);
    img.width = basewidth;
    img.height = hsize;
    return img;
}

$(document).on("change", "#file_image_container", function(e){
    //A new image is being added to the canvas
    var changed_id = parseInt(e.target.id[e.target.id.length - 1]); //Last character of string = identifying number
    var file, img;
    if ((file = e.target.files[0])) {
        img = new Image();
        image_raw = _URL.createObjectURL(file);
        img.onload = function () {
            modifyImage(changed_id, image_raw, 0, 0, this.width, this.height, false, file);
            //Update the image list form to the new values from the image_list variable
            $("#image_list").val(JSON.stringify(image_list));
        };
        img.src = image_raw;
    }

})

//Draws the image in the given context
function depict(image, context) {
    var img = new Image();
    img.src = image.url;
    context.save();
    if ($.inArray(image.url, default_backgrounds) < 0)
    {
        context.shadowBlur=$("#glow_iterations").val();
        context.shadowColor=$("#glow_colour").val();
    }
    context.drawImage(img, image.x, image.y, image.width, image.height);
    context.restore();
};

//Main draw loop
function drawImages(){
    context.clearRect(0, 0, canvas.width, canvas.height);
    depict(canvas_image, context); //Draw the medal frame
    for (var i = 0; i < image_list.length; i++)
    {
        if (! $.isEmptyObject(image_list[i])){
            depict(image_list[i], context);
        }
    }
    //Draw the mask preview if requested
    if (is_previewing_mask){
        context.save();
        var left_width = $("#left_width").val();
        var right_width = $("#right_width").val();
        var top_height = $("#top_height").val();
        var bottom_height = $("#bottom_height").val();
        context.fillStyle = "#000000"

        //Create rectangle fills, order is left, right, top, bottom
        context.fillRect(0, 0, left_width, 600);
        context.fillRect(600, 0, -(600-right_width), 600);
        context.fillRect(0, 0, 600, 600-top_height);
        context.fillRect(0, 600, 600, -bottom_height);
        context.restore();

        //Load the backmask image
        if (custom_mask_image === null)
        {
            context.drawImage(back_mask_image, 300-202, 300-202, 405, 405);
        } else {
            context.drawImage(custom_mask_image, 0, 0, 600, 600)
        }

    }
}

function getMousePos(evt) {
    var canvas = document.getElementById('preview_div');
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

$(document).on("mousedown", "#preview_div", function(e){
    //Determine which image is being dragged
    if (!mouse_pressed){
        mouse_pressed = true;
        if (!dragging_image){
            for (var i = 0; i < image_list.length; i++){
                image = image_list[i];
                if (!image.locked){
                    mouse_pos = getMousePos(e);
                    //Make sure mouse is within the image bounds
                    if ((mouse_pos.x>image.x)&&(mouse_pos.x<image.x+image.width)&&(mouse_pos.y>image.y)&&(mouse_pos.y<image.y+image.height)){
                        dragging_image = true;
                        dragged_image = image;
                        mouse_last_pos = mouse_pos;
                    }
                }
            }
        }
    }
})

//Make sure we listen to the window for mouse up, otherwise we might drag the mouse out of the canvas and mouse up and will still be dragging
$(document).on("mouseup", window, function(e){
    if (mouse_pressed){
        mouse_pressed = false;
        dragging_image = false;
        dragged_image = null;
    }
    //If any values changed during the mouse press, update them
    $("#image_list").val(JSON.stringify(image_list));
})

$(window).bind('mousewheel', function(e){
//Used to rescale an image
    if (dragging_image && mouse_pressed){
        if(e.originalEvent.wheelDelta > 0)
        {
            //mWheelUp
            var new_width = parseInt(dragged_image.width) + 5;
            scale_image(dragged_image, new_width);
        }
        else
        {
            //mWheelDown
            var new_width = parseInt(dragged_image.width) - 5;
            scale_image(dragged_image, new_width);
        }
    }
});

//Firefox 3.x+ compatability for mousewheel
$(window).bind('DOMMouseWheel', function(e){
//Used to rescale an image
    if (dragging_image && mouse_pressed){
        if(e.originalEvent.wheelDelta > 0)
        {
            //mWheelUp
            var new_width = parseInt(dragged_image.width) + 5;
            scale_image(dragged_image, new_width);
        }
        else
        {
            //mWheelDown
            var new_width = parseInt(dragged_image.width) - 5;
            scale_image(dragged_image, new_width);
        }
    }
});

$(document).on("mousemove", window, function(e){
//If we are moving the mouse and clicking/dragging an image, move the image
    if (typeof(mouse_last_pos.x) != 'undefined') {
        if (mouse_pressed && dragging_image){
            current_mouse_pos = getMousePos(e);
            dragged_image.x -= mouse_last_pos.x-current_mouse_pos.x;
            dragged_image.y -= mouse_last_pos.y-current_mouse_pos.y;
            mouse_last_pos = current_mouse_pos;
        }
    }
})

//Hide/unhide advanced feature div
$(document).on("click", "#advanced_features_checkbox", function(e){
    if (showing_advanced_features == false)
    {
        $("#advanced_features").show("fast");
        $("#flat-slider_horizontal").show("fast");
        $("#flat-slider_vertical").show("fast");
        showing_advanced_features = true;
    } else {
        $("#advanced_features").hide("fast");
        $("#flat-slider_horizontal").hide("fast");
        $("#flat-slider_vertical").hide("fast");
        showing_advanced_features = false;
    }
})

$(document).on("change", "#preview_mask", function(){
    is_previewing_mask ^= true; //Toggle the mask using the bitwise or operator
})

function initialise_canvas(){
    canvas = document.getElementById('preview_div');
    context = canvas.getContext('2d');
    setInterval(drawImages, 1000/30); //Setup the main draw loop to run at 30fps, 60fps might cause unwanted strain on the client browser
}

$(document).on("change", "#custom_mask", function() {
    //Adding the custom mask, there is some frontend input validation
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        image_raw = _URL.createObjectURL(file);
        img.onload = function () {
            if ((this.width===600)&&(this.height===600))
            {
                $("#mask_error").html("");
            } else {
                $("#mask_error").html("Custom mask must be 600x600");
            }
        };
        img.src = image_raw;
        custom_mask_image = img;
    }
})


$(document).on("change", "#custom_glow_checkbox", function(e){
    if ($('#custom_glow_checkbox').is(":checked"))
    {
        $("#custom_glow_div").show("fast");
    } else {
        $("#custom_glow_div").hide("fast");
    }
})

$(document).on("click", "#add_image_container", function(e){
    $("#file_image_container").append('<input type="file" id="imgPreview'+image_list.length+'" name="image_file" class="form-control-file text-light"><br>');
    image_list.push({});
})

$(document).on("click", "#remove_image_container", function(e){
    $("#file_image_container").children().last().remove(); //Remove the Breakline + file input div
    $("#file_image_container").children().last().remove();
    image_list.pop();

    //Update image_list since we're removing elements
    $("#image_list").val(JSON.stringify(image_list));
})


$(document).on("change", "#direction_selector", function(){
    let direction = $("#direction_selector").find(":selected").text();
    mask_path = "/static/images/resized_base_mask_"+direction.toLowerCase()+".png";
    back_mask_image.src = mask_path;
    console.log(back_mask_image.src);
})