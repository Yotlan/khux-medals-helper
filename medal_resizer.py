#!/usr/bin/python3.6

"""
The purpose of this script is to resize all medals in the settings.RESIZE_SOURCE_PATH to fit into the largest dimension
image. For example, if there are 3 images of size: 200x100, 210x110, 300x100. All images will be placed in the center
of a new image the size of 300x110.

This essentially normalises the image dimensions of all medals without skewing or deforming the original image.

"""

import os

from PIL import Image

import settings

def scale_image(img, basewidth):
    #Rescale in image maintaining the same ratio based on the width
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((int(basewidth), hsize), Image.ANTIALIAS)
    return img

#Delete spritesheets
spritesheets = os.listdir(settings.SPRITESHEET_PATH)
for sheet in spritesheets:
    os.remove(settings.SPRITESHEET_PATH+"/{}".format(sheet))
#Delete old resized images
original_images = os.listdir(settings.RESIZE_SAVE_PATH)
for image in original_images:
    os.remove(settings.RESIZE_SAVE_PATH+"/{}".format(image))
print(original_images)
medal_images = os.listdir(settings.RESIZE_SOURCE_PATH)

image_list = []

for image in medal_images:
    img = Image.open(settings.RESIZE_SOURCE_PATH+"/{}".format(image))
    image_list.append((img, image))


highest_width = 0
highest_height = 0
for image in image_list:
    if image[0].width > highest_width:
        highest_width = image[0].width
    if image[0].height > highest_height:
        highest_height = image[0].height

for image in image_list:
    new_image = Image.new("RGBA", (highest_width, highest_height))
    new_image.paste(image[0], (int((highest_width-image[0].width)/2), int((highest_height-image[0].height)/2)))
    new_image.save(settings.RESIZE_SAVE_PATH+"/{}".format(image[1]))
