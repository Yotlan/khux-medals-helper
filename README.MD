# KHUX Bot

This project is a forked version based on the original project by its creator [Left](https://gitlab.com/g_). You can find the original version [here](https://gitlab.com/g_/khux-medals-helper).

## Requirements

To begin, execute the following command to install all the required dependencies.

```bash
pip install -r .\requirements.txt --ignore-installed --user
```

Please refrain from modifying the version of Python packages specified in the requirements.txt file.

## Running the Application

Once you have successfully installed all the required Python packages as described in the previous section, you can proceed to execute the following command.

```bash
python .\site_run.py
```

By executing the aforementioned command, you will launch the KHUX Bot, accessible at `localhost:80`. You can now fully immerse yourself in the KHUX Bot experience and make the most of its features!

## Upcoming Features

We are actively working on providing a Docker image for seamless deployment. Once available, you will be able to effortlessly run the KHUX Bot using a simple Docker command, without the need for a separate Python environment. Stay tuned for updates on the Docker image release!