#!/usr/lib/python3.6

import os
import logging.handlers

from flask import Flask, send_from_directory, render_template, g, request, jsonify, abort
from werkzeug.routing import BaseConverter
import sqlite3
from flask_cors import CORS

import settings
from site_api import medal_api
from image_creator.site_image import image_app

appPath = os.path.dirname(__file__)
app = Flask(__name__)
# Use `with app.app_context()`
with app.app_context():
    app.register_blueprint(image_app, url_prefix="/image")
    app.register_blueprint(medal_api, url_prefix='/api')
CORS(app, resources={r"/static/medal_images/*":{"origins":"*"},
                     r"/api/v1/*":{"origins":"*"},
                     r"/static/audio/*":{"origins":"*"},
                     r"/api/v2/*":{"origins":"*"},
                     r"/static/css/*":{"origins":"*"}})

# ### LOGGING CONFIGURATION ### #
LOG_LEVEL = logging.INFO
LOG_FILENAME = os.path.join("logs","main.log")
LOG_FILE_BACKUPCOUNT = 5
LOG_FILE_MAXSIZE = 1024 * 256
# ### END LOGGING CONFIGURATION ### #

# ### LOGGING SETUP ### #
log = logging.getLogger("main")
log.setLevel(LOG_LEVEL)
log_formatter = logging.Formatter('%(levelname)s: %(message)s')
log_formatter_file = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
log_stderrHandler = logging.StreamHandler()
log_stderrHandler.setFormatter(log_formatter)
log.addHandler(log_stderrHandler)
if LOG_FILENAME is not None and __name__ == "__main__":
    log_fileHandler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=LOG_FILE_MAXSIZE, backupCount=LOG_FILE_BACKUPCOUNT)
    log_fileHandler.setFormatter(log_formatter_file)
    log.addHandler(log_fileHandler)
# ### END LOGGING SETUP ### #


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


app.url_map.converters['regex'] = RegexConverter


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(settings.DB_PATH)
    return db

@app.before_request
def pre_request():
    pass

@app.route("/", methods = ["GET"])
def about():
    return render_template("about.html", error="")

# Static Routes
@app.route('/static/css/<regex(".*\.css"):filepath>', methods = ["GET"])
def css(filepath):
    return send_from_directory("static/css", filepath)

@app.route("/static/js/<regex('.*\.js'):filepath>", methods=["GET"])
def js(filepath):
    return send_from_directory("static/javascript", filepath)

@app.route("/static/favicon/<regex('.*\.(png|ico|svg|xml|json)'):filepath>", methods = ["GET"])
def favicon(filepath):
    return send_from_directory("static/favicon", filepath)

@app.route("/static/images/<regex('.*\.(png|jpg|jpeg)'):filepath>", methods = ["GET"])
def image(filepath):
    return send_from_directory("static/images", filepath)

@medal_api.route("/static/medal_images/<regex('.*\.(jpg|png|gif|ico|svg)'):filepath>", methods = ["GET"])
def medal_image(filepath):
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    return send_from_directory("static/medal_images", filepath)

@medal_api.route("/static/audio/<regex('.*\.(mp3|mp4)'):filepath>", methods = ["GET"])
def medal_audio(filepath):
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    return send_from_directory("static/audio", filepath)

@app.errorhandler(403)
def forbidden(e):
    #log.info("{} - 403: {}".format(request.headers["X-Forwarded-For"], request.url))
    return "403 - You are blacklisted."

@app.errorhandler(404)
def page_not_found(e):
    #log.info("{} - 404: {}".format(request.headers["X-Forwarded-For"], request.url))
    return "<h1>404 - Not Found</h1><br>The resource you requested has not been found"

@app.errorhandler(500)
def internal_server_error(e):
    #log.info("{} - 500: {}".format(request.headers["X-Forwarded-For"], request.url))
    if "api" in request.environ["PATH_INFO"].lower() or "static" in request.environ["PATH_INFO"].lower():
        return jsonify({"error":"Internal Server Error"})
    else:
        return "<h1>500 - Internal Server Error</h1><br>Don't worry, probably didn't break anything, server might have recieved unexpected input"

if __name__ == "__main__":
    app.run(host=settings.HOST_IP, port=settings.HOST_PORT)
