from gevent.pywsgi import WSGIServer
from site_main import app
http_server = WSGIServer(("127.0.0.1", 80), app)
http_server.serve_forever()
