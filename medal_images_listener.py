#!/usr/bin/python3.6

import subprocess
import sys
import pyinotify

"""
This script is run to listen to changes to the medal_images directory. If anything is changed, we wun the medal_resizer script.
This way, I can automate changes and updates to the largest medal image and if any new images are added, they will
automatically be resized
"""

class OnWriteHandler(pyinotify.ProcessEvent):
    def my_init(self, cwd, extension, cmd):
        self.cwd = cwd
        self.extensions = extension.split(',')
        self.cmd = cmd

    def _run_cmd(self):
        print ('==> Modification detected')
        subprocess.call(self.cmd.split(' '), cwd=self.cwd)

    def process_IN_MODIFY(self, event):
        if all(not event.pathname.endswith(ext) for ext in self.extensions):
            return
        self._run_cmd()

def auto_compile(path, extension, cmd):
    wm = pyinotify.WatchManager()
    handler = OnWriteHandler(cwd=path, extension=extension, cmd=cmd)
    notifier = pyinotify.Notifier(wm, default_proc_fun=handler)
    wm.add_watch(path, pyinotify.ALL_EVENTS, rec=True, auto_add=True)
    print ('==> Start monitoring %s (type c^c to exit)'.format(path))
    notifier.loop()

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print ("Command line error: missing argument(s).", file=sys.stderr)
        sys.exit(1)

    # Required arguments
    path = sys.argv[1]
    extension = sys.argv[2]

    # Optional argument
    cmd = 'python3'
    if len(sys.argv) == 4:
        cmd = sys.argv[3]

    # Blocks monitoring
    auto_compile(path, extension, cmd)
