import random, os, logging, json, shutil
from logging import handlers

from flask import Blueprint, render_template, request
from PIL import Image

from api_interface import sanatise
import settings, image_creator.image_creator


# ### LOGGING CONFIGURATION ### #
LOG_LEVEL = logging.INFO
LOG_FILENAME = os.path.join("logs","image.log")
LOG_FILE_BACKUPCOUNT = 5
LOG_FILE_MAXSIZE = 1024 * 256
# ### END LOGGING CONFIGURATION ### #

# ### LOGGING SETUP ### #
log = logging.getLogger("image")
log.setLevel(LOG_LEVEL)
log_formatter = logging.Formatter('%(levelname)s: %(message)s')
log_formatter_file = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
log_stderrHandler = logging.StreamHandler()
log_stderrHandler.setFormatter(log_formatter)
log.addHandler(log_stderrHandler)
if LOG_FILENAME is not None and __name__ == "__main__":
    log_fileHandler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=LOG_FILE_MAXSIZE, backupCount=LOG_FILE_BACKUPCOUNT)
    log_fileHandler.setFormatter(log_formatter_file)
    log.addHandler(log_fileHandler)
# ### END LOGGING SETUP ### #

image_app = Blueprint('image_creator', __name__)

def gen_random_id(size):
    string = ""
    for x in range(size):
        string += random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567890")
    return string


def normalise_bounds(bounds, limit=600):
    left = int((limit/settings.MEDAL_CREATOR_DIMENSIONS[0])*(bounds.get("left")-settings.MEDAL_CREATOR_DIMENSIONS[0])+limit)
    right = int((limit / settings.MEDAL_CREATOR_DIMENSIONS[0]) * (bounds.get("right") - settings.MEDAL_CREATOR_DIMENSIONS[0]) + limit)
    top = int((limit / settings.MEDAL_CREATOR_DIMENSIONS[1]) * (bounds.get("top") - settings.MEDAL_CREATOR_DIMENSIONS[1]) + limit)
    bottom = int((limit / settings.MEDAL_CREATOR_DIMENSIONS[1]) * (bounds.get("bottom") - settings.MEDAL_CREATOR_DIMENSIONS[1]) + limit)
    return {"left":left, "right":right,"top":top,"bottom":bottom}

@image_app.route("/list", methods=["GET"])
def list_created_images():
    images = ["/"+entry.path for entry in os.scandir(os.path.join("static", "medal_images")) if entry.is_file() and ".png" in entry.path][:settings.IMAGE_LIST_LIMIT]
    return render_template("image_gallery.html", images=images, error="")

@image_app.route("/create", methods = ["GET"])
def creator():
    return render_template("image_creator.html")

@image_app.route("/create", methods = ["POST"])
def gen_image():
    if request.files:
        files = request.files.getlist("image_file") # List of all images the user has uploaded
        rng_id = gen_random_id(10)
        image_list = json.loads(request.form["image_list"])
        images = []
        os.mkdir("temp/{}".format(rng_id))
        file_count = 0
        for file in files: #Save all the images in the temp path
            file_save_path = os.path.join("temp", rng_id, "{}.png".format(file_count))
            image_file = image_list[file_count]
             #{"file location": String, "width":Integer, "height":Integer, "x":Integer, "y":Integer}
            images.append({"file_location":file_save_path, "x":abs(image_file["x"]), "y":abs(image_file["y"]), "width":abs(image_file["width"]), "height":abs(image_file["height"])})
            file.save(file_save_path)
            file_count += 1
        glow_colour = request.form["glow_colour"][1:]
        glow_colour = tuple(int(glow_colour[i:i + 2], 16) for i in (0, 2, 4))
        glow = {"colour":glow_colour, "intensity":int(request.form["glow_iterations"])}
        frame={"element":sanatise(request.form["element_selector"]), "direction":sanatise(request.form["direction_selector"])}
        try:
            bounds = {
                        "left":int(request.form["left_width"]),
                        "right":settings.MEDAL_CREATOR_DIMENSIONS[0]-int(request.form["right_width"]),
                        "top":settings.MEDAL_CREATOR_DIMENSIONS[1]-int(request.form["top_height"]),
                        "bottom":int(request.form["bottom_height"])
                     }
        except ValueError:
            log.error("Attempted int conversion from different bounds, dumping data: {}, {}, {}, {}".format(request.form["left_width"],
                                                                                                                request.form["right_width"],
                                                                                                                request.form["top_height"],
                                                                                                                request.form["bottom_height"]))
            return render_template("image_creator.html",
                                    error="Invalid custom mask bounds")
        bounds = normalise_bounds(bounds)

        try: #Attempt to create image
            # Layered image object creation
            file_save_path = "static/medal_images/{}".format(rng_id)
            image_creator.image_creator.create_image(images, frame, glow, bounds, file_save_path)
            log.info("Removing working directory: {}".format(rng_id))
            shutil.rmtree(os.path.join("temp", rng_id))
            #Logging things
            log.info("{} created an image with {}, new name: {}".format(request.remote_addr, images, rng_id))
            return render_template("image_display.html", image="/static/medal_images/{}.png".format(rng_id))
        except ValueError as e:
            return render_template("image_creator.html", error=e.__str__())
        except Exception as e:
            log.error("Failed to create image with error: {}, dumping data: {} {}".format(e, images, frame))
            raise Exception(e)
    else:
        return render_template("image_creator.html", error="No Image uploaded")
