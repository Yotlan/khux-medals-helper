#!/usr/bin/python3.6


import os
import time
import platform
import datetime

base_path = os.getcwd() + "/static/images"
paths = os.listdir(base_path)

"""
This script basically checks every file in the given base path if it is older than a day, if it is, delete it.
"""

print(datetime.datetime.now().isoformat()+" - "+str(paths))

for image in paths:
    if platform.system() == "Windows":
        age = os.path.getctime(base_path+image)
    else:
        stat = os.stat(base_path+image)
        age = stat.st_mtime
    if time.time()-age > 60*60*24:
        print("removing image {}".format(image))
        os.remove(base_path+image)
