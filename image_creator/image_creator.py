from PIL import Image, ImageFilter
import settings


IMAGE_PATH = "static/images/{}"



"""
Requirements

Main method accepts:
images: List of dicts, each dict contains {"file location": String, "width":Integer, "height":Integer, "x":Integer, "y":Integer}
frame: dict {"direction": String, "element": String}
bounds: dict {"top":Integer, "bottom" :Integer, "left":Integer,  "right":Integer}
glow: dict {"colour": List, "intensity": Integer}

"""
def get_frame(direction, element):
    return Image.open(IMAGE_PATH.format("resized_{}_{}_7.png".format(direction, element)))

def get_base_mask(direction):
    return Image.open(IMAGE_PATH.format("resized_base_mask_{}.png".format(direction)))

def check_images(images):
    for image in images:
        img = Image.open(image.get("file_location"))
        if img.mode != "RGBA":
            return False
    return True


def create_image (images:list, frame:dict, glow:dict, bounds:dict, save_path):
    # The main image creation method, this is called first with all the required values
    if check_images(images):
        frame_image = get_frame(frame.get("direction"), frame.get("element"))
        blank = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
        mask = create_mask(bounds, frame.get("direction"))
        composite = composite_images(images)

        # Applies the glow to the provided images
        glow = create_glow(composite, glow)

        # This part "cuts" out the unwanted parts from the frame with the created mask
        masked_composite = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
        masked_composite.paste(glow, mask=mask)

        # Now put everything togeter
        blank.alpha_composite(frame_image, (int(frame_image.width/4), int(frame_image.height/4)))
        blank.alpha_composite(masked_composite)
        blank.save(save_path+".png")
    else:
        raise ValueError("All supplied images must be RGBA (Make sure all your images have an alpha channel)")


def create_bounds(bounds:dict):
    #Makes sure that all the values are positive and withing the size of the medal dimensions
    bounds = {x:abs(y) for x,y in bounds.items() if y <= settings.MEDAL_CREATOR_DIMENSIONS[0]}
    img = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
    try:
        for x in range(bounds.get("left")):
            for y in range(img.height):
                img.putpixel((x,y), (0,0,0,255))
        for x in range(img.height):
            for y in range(bounds.get("top")):
                img.putpixel((x,y), (0,0,0,255))
        for x in range(img.width - bounds.get("right"), img.width):
            for y in range(img.height):
                img.putpixel((x,y), (0,0,0,255))
        for x in range(img.width):
            for y in range(img.height - bounds.get("bottom"), img.height):
                img.putpixel((x,y), (0,0,0,255))
    except KeyError:
        raise KeyError("Improper bounds in: create_bounds({})".format(bounds))
    return img

def create_mask(bounds, direction):

    blank = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
    frame_mask = get_base_mask(direction)
    blank.alpha_composite(frame_mask, (int(frame_mask.width/4), int(frame_mask.height/4)))
    bounds_mask = create_bounds(bounds)
    blank.alpha_composite(bounds_mask)
    return blank

def resize(img, width, height):
    """
    Resizes the given image to the specified dimensions. It first checks if the dimensions are too large, if it is, then
    we set to the the maximum instead

    :param img:
    :param dimensions:
    :return:
    """
    if width > settings.MEDAL_CREATOR_DIMENSIONS[0]:
        width = settings.MEDAL_CREATOR_DIMENSIONS[0]
    if height > settings.MEDAL_CREATOR_DIMENSIONS[1]:
        height = settings.MEDAL_CREATOR_DIMENSIONS[1]
    return img.resize((width, height))

def composite_images(images):
    """
    Composites the list of images to their positions on the canvas, it also applies the resizing
    :param images: 
    :return: 
    """
    img = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
    for image in images:
        i = Image.open(image.get("file_location"))
        i = resize(i, image.get("width"), image.get("height"))
        img.alpha_composite(i, (image.get("x"), image.get("y")))
    return img

def create_glow(image, glow):
    # Creates the shadow glow of each medal
    data = []
    img = Image.new("RGBA", settings.MEDAL_CREATOR_DIMENSIONS)
    for pixel in image.getdata():
        if pixel[3] > 0:
            data.append(tuple(glow.get("colour")))
        else:
            data.append(pixel)
    img.putdata(data)
    img = img.filter(ImageFilter.GaussianBlur(glow.get("intensity")))
    img.alpha_composite(image)
    return img

if __name__ == "__main__":
    # Usage example
    img = [{"file_location":"ryuuko.png", "x":250, "y":100, "width":200, "height":400},
           {"file_location":"yuna.png", "x":200, "y":200, "width":150, "height":350}]
    glow = {"colour":[255,0,0], "intensity":3}
    frame = {"direction":"reversed","element":"magic"}
    bounds = {"left":100, "right":100, "top":300, "bottom":0}
    create_image(img, frame, glow, bounds)
