url = "https://docs.google.com/spreadsheets/d/{}/export?format=xlsx"

import requests
import openpyxl
import io
import os
from database import Database
import settings

db = Database()
conn = db.connection(db.dbname)


if __name__ == "__main__":
    cursor = conn.cursor()
    r = requests.get(url.format(settings.SPREADSHEET_DATA))
    wb = openpyxl.load_workbook(io.BytesIO(r.content))

    sql = """INSERT INTO medal(medal_id, medal_name, medal_rarity, medal_type, medal_element, medal_direction, medal_tier, medal_targets, 
                medal_attack, medal_defence, medal_cost, medal_multiplier, medal_hits, medal_notes, medal_region, medal_image_link,
                medal_pullable, medal_voice_link) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
                """

    cursor.execute("DELETE FROM medal;")
    for row in list(wb.active)[1:]: #Load JP medals
        row = [x.value for x in row]
        cursor.execute(sql, (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10],
                             row[11], row[12], row[13], row[14], row[15], row[16], row[17]))
    conn.commit()
