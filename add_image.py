import requests
from bs4 import BeautifulSoup


base_url = "http://www.khunchainedx.com{}"

url = input("Medal url: ")
name = input("Medal name: ")

response = requests.get(url)

# Get the hi res image link, this shouldn't need changing unless they decide to redesign the entire site
soup = BeautifulSoup(response.content)
images = soup.find("table", {"class":"goright"}).find_all("a", {"class":"image"})
img_link = images[2].find("img")["src"]


#Save the image
response = requests.get(base_url.format(img_link), stream=True)
with open("static/medal_images/{}.png".format(name), "wb") as fp:
    for chunk in response.iter_content(chunk_size=128):
        fp.write(chunk)

print("{}"+"/{}.png".format(name))
