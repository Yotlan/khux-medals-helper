import logging.handlers
import logging, os
from json.decoder import JSONDecodeError
import hashlib

import sqlite3
from flask import Blueprint, g, render_template, request, jsonify, json

import settings
import api_interface as interface

medal_api = Blueprint('medal_api', __name__)


# ### LOGGING CONFIGURATION ### #
LOG_LEVEL = logging.INFO
LOG_FILENAME = os.path.join("logs","api.log")
LOG_FILE_BACKUPCOUNT = 5
LOG_FILE_MAXSIZE = 1024 * 256
# ### END LOGGING CONFIGURATION ### #

# ### LOGGING SETUP ### #
log = logging.getLogger("api")
log.setLevel(LOG_LEVEL)
log_formatter = logging.Formatter('%(levelname)s: %(message)s')
log_formatter_file = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
log_stderrHandler = logging.StreamHandler()
log_stderrHandler.setFormatter(log_formatter)
log.addHandler(log_stderrHandler)
if LOG_FILENAME is not None and __name__ == "__main__":
    log_fileHandler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=LOG_FILE_MAXSIZE, backupCount=LOG_FILE_BACKUPCOUNT)
    log_fileHandler.setFormatter(log_formatter_file)
    log.addHandler(log_fileHandler)
# ### END LOGGING SETUP ### #


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(settings.DB_PATH)
    return db

def medal_handler_2(request_object):
    try:
        raw_input = json.loads(request_object.data)
    except JSONDecodeError:
        return {"error":"Invalid JSON"}
    buff = raw_input.get("buff")
    format = raw_input.get("format")
    filter = raw_input.get("filter")
    # Removes bad characters
    buff = interface.sanatise(buff)
    format = interface.sanatise(format)
    filter = interface.sanatise(filter)
    if not filter:
        return {"error":"Must specify a filter"}
    if raw_input.get("type") == "spritesheet":
        name = hashlib.md5(request_object.data).hexdigest()
        if os.path.exists(os.path.join("static", "images", "spritesheets", name+".json")):
            data = json.load(open(os.path.join("static", "images", "spritesheets", name+".json"), "r"))
        else:
            data = interface.generate_spritesheet(get_db(), filter, buff, name)
    elif raw_input.get("type") == "buff":
        data = {"error":"Not yet implemented"}
    else:
        data = interface.get_medal_2(get_db(), format, filter, buff)
    return data


def medal_handler(request_object):
    query = request_object.args.get("q")
    if query == "names":
        return interface.get_names(get_db())
    elif query == "images":
        return interface.get_image_locations(get_db())
    elif query == "data":
        name = request_object.args.get("medal")
        if name:
            name = interface.sanatise(name).lower()
        filter = request_object.args.get("filter")
        format = request_object.args.get("format")
        buff = request_object.args.get("buff")
        try:
            buff = interface.sanatise(json.loads(buff))
        except Exception as e:
            if buff:
                interface.eprint(e)
                return {"error": "Failed to parse buff"}
        try:
            filter = interface.sanatise(json.loads(filter))
        except Exception as e:
            if filter: #Ignore if filter is None
                interface.eprint(e)
                return {"error":"Failed to parse filter"}
        try:
            format = interface.sanatise(json.loads(format)["format"])
        except Exception as e:
            if format: #Ignore if format is None
                interface.eprint(e)
                return {"error":"Failed to parse format"}
        return interface.get_medal(get_db(), name=name, filter=filter, format=format, buff=buff)
    else:
        return {"error":"Invalid query"}


@medal_api.route("/v1/<method>")
def api(method):
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    if method == "medal":
        data = medal_handler(request)
    else:
        data = {"error": "invalid method parameter"}
    return jsonify(data)

@medal_api.route("/v2/<method>", methods=["POST"])
def api_2(method):
    #log.info("{} - Requested: {}, Data: {}".format(request.headers["X-Forwarded-For"], request.url, request.data))
    if method == "get":
        data = medal_handler_2(request)
    else:
        data = {"error": "Invalid method parameter"}
    return jsonify(data)

@medal_api.route("/docs/filters")
def filter_documentation():
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    return render_template("medal_data.html")

@medal_api.route("/docs/buffs")
def buffs_documentation():
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    data = interface.get_buffs(get_db())
    return render_template("medal_buffs.html", data = data)

@medal_api.route("/docs")
def docs():
    #log.info("{} - Requested: {}".format(request.headers["X-Forwarded-For"], request.url))
    return render_template("api_documentation.html")